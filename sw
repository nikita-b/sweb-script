#!/bin/bash
# Скрипт подключения к серверам компании sweb
# alias sw="~/sw"

SCRIPT=`basename "$0"` # Имя скрипта. Используется для отображения в разделе помощи.
NAME=`whoami` # Имя пользователя скрипта. Используется при подключении к серверам. Можно переопределить при необходимости.

if [ "$1" == "--help" ] || [ -z "$1" ] ; then
 	echo "Формат: ${SCRIPT} [type] номер_сервера -u [Пользователь]"
	echo "        ${SCRIPT} 12  - подключение к VH серверу 12"
	echo "        ${SCRIPT} vip 4 - подключение к VIP серверу 4"
	echo "        ${SCRIPT} vh 10 -d [строка] выводит список доменов пользователя и папки где они лежат"
	echo " 		  На самом деле -d просто грепает по файлу vhosts.map т.е можно вводить и домен и логин"
	exit 0
fi

TYPE_SERVER=$1
NUMBER_SERVER=$2

if [[ "$1" =~ ^[0-9]{1,10}$ ]]; then
	NUMBER_SERVER=$1
	TYPE_SERVER="vh"
else
	shift
fi

shift

while getopts ":d:" opt; do
    case $opt in
		d)
			ssh ${NAME}@${TYPE_SERVER}${NUMBER_SERVER}.sweb.ru /bin/bash << EOF
				cat /etc/apache2/vhosts.map | grep $OPTARG | awk '{t = \$1; \$1 = \$2; \$2 = t; print; } ' | grep -v ".swtest.ru$";
EOF
			;;
        \?)
            echo "Опции не существует: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Опция -$OPTARG требует указания аргумента." >&2
            exit 1
            ;;		    	
    esac
done

#Подключение к серверу
ssh ${NAME}@${TYPE_SERVER}${NUMBER_SERVER}.sweb.ru
