#!/usr/bin/perl

use URI::UTF8::Punycode;
use Term::ANSIColor;
use Term::ANSIColor qw(:constants);
use encoding UTF8;
use strict;

sub puny_encode_domain{
    my $domain = shift;

    my @puny_words = ();
    foreach my $d (split(/\./, $domain)) {
        if ($d =~ m/[а-яА-Я]/i) {
            $d = puny_enc($d);
        
        }
        push(@puny_words, $d);
    }
    my $result = join('.', @puny_words);
    return $result;
}

sub puny_decode_domain{
    my $domain = shift;

    my @puny_words = ();
    foreach my $d (split(/\./, $domain)) {
        if ($d !~ m/[а-яА-Я]/i) {
            $d = puny_dec($d);
        }
     push(@puny_words, $d);
    }
    my $result = join('.', @puny_words);
    return $result;
}

sub output_whois{
    my @i=split(/\n/,@_[0]);
    
    foreach my $i(@i){
        $i =~ s/ns1\.spaceweb\.ru\.?/\x1b[1m\x1b[33mns1\.spaceweb\.ru\.\x1b[0m/gi;
        $i =~ s/ns2\.spaceweb\.ru\.?/\x1b[1m\x1b[33mns2\.spaceweb\.ru\.\x1b[0m/gi;
        $i =~ s/SWEB-MNT-GPT/\x1b[1m\x1b[33mSWEB-MNT-GPT\x1b[0m/g;
        $i =~ s/SPACEWEB-MNT-GPT/\x1b[1m\x1b[33mSPACEWEB-MNT-GPT\x1b[0m/g;
        $i =~ s/R01-REG-RF/\x1b[33mR01-REG-RF\x1b[0m/g;
        $i =~ s/R01-REG-RIPN/\x1b[33mR01-REG-RIPN\x1b[0m/g;
        $i =~ s/ONLINENIC/\x1b[33mONLINENIC\x1b[0m/g;
        $i =~ s/OnlineNIC/\x1b[33mOnlineNIC\x1b[0m/g;
        $i =~ s/Spaceweb JSC/\x1b[33mSpaceweb JSC\x1b[0m/gi;
        $i =~ s/JSC "?SpaceWeb"?/\x1b[33mJSC SpaceWeb\x1b[0m/g;
        $i =~ s/NOT DELEGATED/\x1b[1m\x1b[31mNOT DELEGATED\x1b[0m/gi;
        $i =~/\x1b\[/i ||$i =~ s/DELEGATED/\x1b[1m\x1b[32mDELEGATED\x1b[0m/gi;
        $i =~ s/NOT DELEGATED/\x1b[1m\x1b[31mNOT DELEGATED\x1b[0m/gi;
        $i =~ s/Status: ok/Status:\x1b[1m\x1b[32mOK\x1b[0m/gi;
        $i =~ s/redemptionPeriod/\x1b[1m\x1b[31mredemptionPeriod\x1b[0m/gi;
        $i =~ s/(CLIENT )?(HOLD)/\x1b[1m\x1b[31m$1$2\x1b[0m/i;
        $i =~ s/(@_[1])/\x1b[1m$1\x1b[0m/gi;
        print "$i\n";

    }

}

my $domain;
foreach my $i(@ARGV){
    if ($i !~ /^-\w+/i){
        chomp($i);$i=lc $i;
        $domain=$i;
        if($domain =~ /[а-яА-Я]/i){
            $domain=puny_encode_domain($domain);
            print BOLD,"$i -> $domain\n",RESET;
            $i=$domain;last;
        }elsif($domain =~ /^xn--.*$/i){
            print BOLD,"$i --> ",puny_decode_domain($domain),RESET,"\n";
        }
    }
}
my $param=join(' ',@ARGV);
my $host="";

if($domain =~ /\.(ru|su)$/i){ $host="-hwhois.r01.ru";}
if($domain =~ /^.*\.(spb|msk|org|com|net|sochi|edu|int)\.(ru|su)$/i){  $host="-hwhois.nic.ru";}
if($domain =~ /^.*\.xn--p1ai$/i){$host="-hwhois.r01.ru";};

my $res= `/usr/bin/whois $host $param `; 
output_whois($res,$domain);
