fullpath=`pwd`
echo Полный путь к файлам: $fullpath

echo -n "Выберите название для проекта (по умолчанию Site):"
read project_name
if [ -z "$project_name" ]; then
        project_name="Site"
fi
echo "[Название проекта $project_name]"


echo -n "Производим установку виртуального окружения...."
virtualenv .djangovenv > /dev/null
echo "OK"

source .djangovenv/bin/activate

echo -n "Начинаем установку Django...."
pip install django==1.7.1 > /dev/null
echo "OK"

echo -n "Создаем проект...."
django-admin.py startproject $project_name && deactivate
echo OK

rm -rf public_html/
mv $project_name/ public_html

echo -n "Записываем файл .htaccess...."
echo "AddDefaultCharset utf-8
AddHandler wsgi-script .py
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ /${project_name}/wsgi.py/\$1 [QSA,L]" > $fullpath/public_html/.htaccess
echo "OK"
echo -n "Делаем резервную копию wsgi.py...."
mv $fullpath/public_html/$project_name/wsgi.py $fullpath/public_html/$project_name/wsgi.py_bak
echo "OK"
echo -n "Заменяем файл wsgi.py...."
echo "import os, sys

sys.path.insert(0, '$fullpath/public_html')
sys.path.insert(0, '$fullpath')
sys.path.insert(0, '$fullpath/.djangovenv/lib64/python2.7/site-packages')

os.environ['DJANGO_SETTINGS_MODULE'] = '${project_name}.settings'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()" > $fullpath/public_html/$project_name/wsgi.py
echo "OK"
echo "--------"
echo "Установка завершена. Не забудьте сменить версию PHP"






